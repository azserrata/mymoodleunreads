/* My Moodle Unread Messages
 * version 0.1
 * 2013-10-26
 * Copyright (c) 2013,
 *                    David Fernandes <david.paiva.fernandes@gmail.com>
 * Released under the GPL license
 * http://www.gnu.org/copyleft/gpl.html
 *
 * --------------------------------------------------------------------
 *
 * This is a Greasemonkey user script.
 *
 * To install, you need Greasemonkey:
 *              https://addons.mozilla.org/en-US/firefox/addon/748
 * Then restart Firefox and revisit (load file...) this script.
 * Under Tools, there will be a new menu item to "Install User Script".
 * Accept the default configuration and install.
 *
 * To uninstall, go to Tools/Manage User Scripts,
 * select "Hello World", and click Uninstall.
 *
 * Portuguese instructions / instruções em Português:
 *
 * Este é um script (em Javascript) para ser utilizado pela extensão/add-on
 * Greasemonkey, que pode ser obtido no seguinte link:
 *              https://addons.mozilla.org/en-US/firefox/addon/748
 * Após instalação do Greasemonkey, basta carregar o script (abrir ficheiro...),
 * para que lhe seja pedido para efectuar a respectiva instalação do mesmo.
 * Deste modo, a sua lista de UCs no Moodle pode ser personalizada a gosto!
 *
 * --------------------------------------------------------------------
 */
// ==/UserScript==
// @name          My Moodle Unread Messages
// @namespace     http://elearning.uab.pt
// @description   Puts unread messages info+link along on the UC's list
// @include       http://elearning.uab.pt/my/*
// @exclude       *
// ==/UserScript==

//var script = document.createElement('script');
//script.src = 'http://jqueryjs.googlecode.com/files/jquery-1.2.6.min.js';
//script.type = 'text/javascript';
//document.getElementsByTagName('head')[0].appendChild(script);

// version name

var BC = document.getElementsByClassName("breadcrumb")[0];
var Version = document.createTextNode("MyMoodleUnreads v.0.2");
BC.appendChild(Version);

var ucs = new ListOfMyUCs();
ucs.populate();

function ListOfMyUCs() {

    this.populate = function() {
	var CourseListBlock = document.getElementsByClassName("block_course_list  block list_block")[0];
	if(CourseListBlock!=null) {
	    var CourseList = CourseListBlock.getElementsByClassName("unlist")[0];
	    var UCs = CourseList.getElementsByClassName("column c1");
	    if(UCs!=null) {
		for(var idx in UCs) {
		    var UC = UCs[idx];
		    if(UC!=null) {
			var UCAnchor = UC.getElementsByTagName("a")[0];
			var UCHRef = UCAnchor.getAttribute("href");
			var UCText = UCAnchor.innerHTML.split(">")[1];
			addUnreadMessagesInformation(UC, UCHRef);
		    }
		}
	    }
	}
    }
}

function addUnreadMessagesInformation (node, url) {
    var newDiv = document.createElement("div");
    var newUL = document.createElement("ul");
    newDiv.appendChild(newUL);
    node.appendChild(newDiv);

    callAjax(url, function(msg) { 
	var doc = createDocument(msg, "title");
	var Activities = doc.getElementsByClassName("activityinstance");
	for(var idx in Activities) {
	    var Activity = Activities[idx];
	    var Instance = Activity.getElementsByClassName("instancename")[0];
	    if(Instance!=null) {
		var ForumName = Instance.innerHTML.split('<')[0];
		var Unread = Activity.getElementsByClassName("unread")[0];
		if(Unread!=null) {
		    var Message = Unread.getElementsByTagName("a")[0];
		    if(Message!=null) {
			var ForumUrl = Message.getAttribute("href");
			var UnreadMessages = Message.innerHTML;
			var newLink = document.createElement("a");
			newLink.setAttribute("href",ForumUrl);
			var newContent = document.createTextNode(UnreadMessages);
			newLink.appendChild(newContent);
			var newLI = document.createElement("li");
			newLI.appendChild(document.createTextNode(ForumName + " - "));
			newLI.appendChild(newLink);
			newUL.appendChild(newLI);
		    }
		}
	    }
	}
    } );
}

function createDocument(html, title) {
  var doc = document.implementation.createHTMLDocument(title)
  doc.documentElement.innerHTML = html
  return doc
}

function callAjax(url, callback){
    var xmlhttp;
    xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function(){
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
            callback(xmlhttp.responseText);
        }
    }
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

